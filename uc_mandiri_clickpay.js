(function ($) {
  Drupal.behaviors.uc_mandiri_clickpay = {
    attach: function (context, settings) {         
      
      // handle debit card validation
      $('.debit-card-textfield').blur(function(){
        if($(this).val() == '' || $(this).val().length != 16){
          alert('Please enter your 16 Digit Mandiri Debit Card Number');
        }
        else {
          var debit_number = $('.debit-card-textfield').val();
          $('.mandiri-debit-card-textfield').val(debit_number.slice(- 10));
        }        
      });
      
      $('.mandiri-debit-card-textfield').blur(function(){
        if($('.debit-card-textfield').val() == '' || $('.debit-card-textfield').val().length != 16){
          alert('Please enter your 16 Digit Mandiri Debit Card Number');
        }
        else {
          var debit_number = $('.debit-card-textfield').val();
          $('.mandiri-debit-card-textfield').val(debit_number.slice(- 10));
        } 
      });
    }
  };
})(jQuery);