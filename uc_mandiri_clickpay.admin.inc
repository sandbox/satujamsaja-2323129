<?php

/**
 * @file
 * Mandiri Clickpay Configuration
 */
function uc_mandiri_clickpay_admin_settings_form($form, &$form_state) {

  $form['mandiri_clickpay'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mandiri Clickpay Configuration Page'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['mandiri_clickpay']['uc_mandiri_clickpay_merchant_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandiri Clickpay Merchant Username'),
    '#default_value' => variable_get('uc_mandiri_clickpay_merchant_username', 'user'),
    '#description' => t('Mandiri clickpay merchant username to access API'),
  );

  $form['mandiri_clickpay']['uc_mandiri_clickpay_merchant_password'] = array(
    '#type' => 'password',
    '#title' => t('Mandiri Clickpay Merchant Password'),
    '#attributes' => array('value' => variable_get('uc_mandiri_clickpay_merchant_password', '')),
    '#description' => t('Mandiri clickpay merchant password to access API'),
  );

  $form['mandiri_clickpay']['uc_mandiri_clickpay_payment_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandiri Clickpay Payment API URL'),
    '#description' => t('Mandiri clickpay payment API URL'),
    '#default_value' => variable_get('uc_mandiri_clickpay_payment_server_url', 'http://127.0.0.1:22334/velispayment/'),
  );

  $form['installment']['uc_mandiri_clickpay_installment_options'] = array(
    '#type' => 'textarea',
    '#title' => t('Mandiri Installment Options'),
    '#description' => t('Enter installment options per line. Dont use ( ) characters.'),
    '#default_value' => variable_get('uc_mandiri_clickpay_installment_options', '; example' . PHP_EOL . '; Mandiri = 3,6,12 '),
  );

  $form['installment']['uc_mandiri_clickpay_enable_installment_options'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Installment Options'),
    '#description' => t('Enable Installment options for credit card.'),
    '#default_value' => variable_get('uc_mandiri_clickpay_enable_installment_options', '1'),
  );

  return system_settings_form($form);
}
